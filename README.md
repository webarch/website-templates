# Webarchitects Website Templates

The SSI header and footers, CSS and images for the [Webarchitects
website](https://www.webarchitects.coop/) and the default pages for the Webarch
Hosting servers etc. The [content of the pages is in the website
repo](https://git.coop/webarch/website).

See also the [.htaccess](https://docs.webarch.net/wiki/HTAccess) notes on the
[Webarchitects documentation wiki](https://docs.webarch.net/wiki/Main_Page).

## Apache Configuration

On shared hosting servers this repo is checked out at `/var/www/wsh` and the
following Apache configuration is used with these templates (the actual
configuration isn't here as it is a Ansible template), note that the
`.htaccess` file for [the
website](https://git.coop/webarch/website/blob/master/.htaccess) overides some
of the following env vars:

```apache
# Webarchitects HTML/CSS
Alias /wsh /var/www/wsh
Alias /wsh.shtml /var/www/wsh/wsh.shtml
# icons for directory index icons
Alias /icons /usr/share/apache2/icons
# env vars for the default index page, wsh.shtml
SetEnv HOME_DOMAIN "webarch.net"
SetEnv SITE_TITLE "Webarchitects Shared Hosting"
SetEnv STATIC_DIR "/wsh/"
SetEnv STATIC_TOP "top.shtml"
SetEnv STATIC_BOT "bot.shtml"
# default page title 
SetEnv PAGE_TITLE "Directory Listing"
SetEnv PAGE_DESC  ""
<Directory "/var/www">
  Options IncludesNoExec
  DirectoryIndex wsh.shtml
  AddOutputFilter Includes shtml
  <IfModule mod_version.c>
    <IfVersion >= 2.4>
      SSILegacyExprParser on
    </IfVersion>
  </IfModule>
  <IfModule !mod_version.c>
    <IfModule mod_authz_core.c>
      SSILegacyExprParser on
    </IfModule>
  </IfModule>
  <IfModule mod_version.c>
    <IfVersion >= 2.2>
      SSILastModified on
    </IfVersion>
  </IfModule>
  HeaderName /wsh/top.shtml
  ReadmeName /wsh/bot.shtml
  IndexOptions FancyIndexing VersionSort HTMLTable NameWidth=* DescriptionWidth=*
  IndexOptions Charset=UTF-8 SuppressHTMLPreamble XHTML TrackModified IconsAreLinks
  IndexOptions SuppressDescription
  AllowOverride None
  <IfModule mod_version.c>
    <IfVersion < 2.4>
      Order Allow,Deny
      Allow from all
    </IfVersion>
    <IfVersion >= 2.4>
      Require all granted
    </IfVersion>
  </IfModule>
  <IfModule !mod_version.c>
    <IfModule !mod_authz_core.c>
      Order Allow,Deny
      Allow from all
    </IfModule>
    <IfModule mod_authz_core.c>
      Require all granted
    </IfModule>
  </IfModule>
  <IfModule mod_expires.c>
    ExpiresActive On
    ExpiresDefault A1209600
    ExpiresByType text/html "now plus 5 minutes"
    ExpiresByType image/gif "access plus 1 years"
    ExpiresByType image/jpg "access plus 1 years"
    ExpiresByType image/jpeg "access plus 1 years"
    ExpiresByType image/png "access plus 1 years"
    ExpiresByType image/vnd.microsoft.icon "access plus 1 years"
    ExpiresByType image/x-icon "access plus 1 years"
    ExpiresByType image/ico "access plus 1 years"
    ExpiresByType application/javascript "now plus 1 week"
    ExpiresByType application/x-javascript "now plus 1 week"
    ExpiresByType text/javascript "now plus 1 week"
    ExpiresByType text/css "now plus 1 week"
  </IfModule>
</Directory>
ErrorDocument 404 /wsh/404.shtml
ErrorDocument 401 /wsh/401.shtml
ErrorDocument 500 /wsh/500.shtml
ErrorDocument 403 /wsh/403.shtml
```

